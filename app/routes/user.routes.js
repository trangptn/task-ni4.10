const express = require("express");

const route = express.Router();

const userMiddleWare = require("../middlewares/user.middleware");
const userController = require("../controllers/user.controller");

route.get("/", userController.getAllUser);
route.post("/", userMiddleWare.verifyToken, userController.createUser);
route.put("/:userid", [userMiddleWare.verifyToken, userMiddleWare.checkUser], userController.updateUser);
route.delete("/:userid", [userMiddleWare.verifyToken, userMiddleWare.checkUser], userController.deleteUser);

module.exports = route;