const mongoose = require('mongoose');

const refeshTokenSchema= new mongoose.Schema({
    token:{
        type:String,
        required:true
    },
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    expiredDate:{
        type:Date,
        required:true
    }
})

module.exports=mongoose.model("RefreshToken",refeshTokenSchema);