const mongoose = require("mongoose");

mongoose.Promis = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.refreshToken=require("./refeshtoken.model");

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;